# tuitube

## Instructions

#### Prebuilts

Available under CI artifacts, hit the green arrow button.

#### Compile

```bash
go get -u gitlab.com/diamondburned/tuitube

# For convenience
PATH=$GOPATH/bin:$PATH
```

## Configs

#### Preferred Player

`PLAYER=mpv ./tuitube`

## Keybinds

- Tab cycles focus

#### Search bar

- Down arrow: move to video list

#### Video list

- Up/Down arrow: browse
- Enter: open with `mpv`
- Right arrow: open in browser
- `q` quits

## Todo

- [ ] Implement sixel/`w3m`/`feh` (configurable)
- [ ] Add Windows named FIFO support (ETA: never)
- [ ] Make a custom element instead of the ugly List
- [ ] Proper warn dialog instead of `safePanic`

