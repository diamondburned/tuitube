package main

import (
	"fmt"
	"io"
	"net/http"

	"github.com/rylio/ytdl"
)

func downloadFormat(video *ytdl.VideoInfo, vfmt ytdl.Format) io.ReadCloser {
	u, err := video.GetDownloadURL(vfmt)
	if err != nil {
		safePanic(err)
	}

	resp, err := http.Get(u.String())
	if err != nil {
		safePanic(err)
	}

	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		safePanic(fmt.Errorf("Invalid status code: %d", resp.StatusCode))
	}

	return resp.Body
}

func safePanic(err error) {
	app.Stop()
	panic(err)
}
