package main

import (
	"fmt"
	"io"
	"os"
	"os/exec"
	"sync"

	"github.com/rylio/ytdl"
)

// PreferredFormat matches the containers
// possible value: webm|mp4
const PreferredFormat = "webm"

var PreferredPlayer = "mpv"

func play(url string) {
	video, err := ytdl.GetVideoInfo(url)
	if err != nil {
		app.Stop()

		fmt.Println(url)
		panic(err)
	}

	var (
		audiofifo = "/tmp/audio" + video.ID
		videofifo = "/tmp/video" + video.ID
		wg        = &sync.WaitGroup{}
	)

	wg.Add(2)

	go FIFOAudio(audiofifo, video, wg)
	go FIFOVideo(videofifo, video, wg)

	trans := exec.Command(
		"ffmpeg",
		"-nostdin",
		"-i", videofifo,
		"-i", audiofifo,
		"-f", "matroska",
		"pipe:1",
	)

	mpv := exec.Command(PreferredPlayer, "-")

	pipeout, pipein := io.Pipe()

	mpv.Stdin = pipeout
	trans.Stdout = pipein

	if err := trans.Start(); err != nil {
		safePanic(err)
	}

	if err := mpv.Start(); err != nil {
		safePanic(err)
	}

	trans.Wait()

	// not checking for errs to
	// prevent SIGINT from user
	mpv.Wait()

	// Clean-up the FIFOs

	os.Remove(videofifo)
	os.Remove(audiofifo)
}
