package main

import "github.com/rivo/tview"

var (
	cyclePrimitives  []*tview.Primitive
	cyclePrimCurrent int
)

func addPrimitive(p tview.Primitive) {
	cyclePrimitives = append(cyclePrimitives, &p)
}

func cyclePrimitive() {
	cyclePrimCurrent++
	if cyclePrimCurrent == len(cyclePrimitives) {
		cyclePrimCurrent = 0
	}

	app.SetFocus(
		*cyclePrimitives[cyclePrimCurrent],
	)
}
