package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"sync"
	"time"
)

type Progress string

const (
	// ProgressContinue is printed when encoding
	ProgressContinue Progress = "continue"

	// ProgressEnd is called when encoding is finished
	ProgressEnd Progress = "end"

	ProgressUnknown Progress = "?"
)

type FFMpegProgress struct {
	BitRate    string
	TotalSize  int64
	OutTimeus  int64
	OutTimems  int64
	OutTime    string
	DupFrames  uint64
	DropFrames uint64
	Speed      float64
	Percentage float64
	Progress   Progress
}

func encodeMe(trans *exec.Cmd, resp *http.Response, filename string) {
	printfn := func(f string, s ...interface{}) {
		if filename != "-" {
			fmt.Printf(f, s...)
		}
	}

	var writeToMe = os.Stdout

	if filename != "-" {
		f, err := os.OpenFile(
			filename,
			os.O_RDWR|os.O_TRUNC|os.O_CREATE|os.O_SYNC,
			os.ModePerm,
		)

		if err != nil {
			panic(err)
		}

		defer f.Close()

		writeToMe = f
	}

	stderr, err := trans.StderrPipe()
	if err != nil {
		panic(err)
	}

	stdout, err := trans.StdoutPipe()
	if err != nil {
		panic(err)
	}

	scanner := bufio.NewScanner(stderr)

	syncwg := sync.WaitGroup{}
	syncwg.Add(1)

	defer syncwg.Wait()

	progress := FFMpegProgress{}

	go func() {
		defer stderr.Close()
		defer syncwg.Done()

		for scanner.Scan() {
			if kv := strings.Split(scanner.Text(), "="); len(kv) == 2 {
				switch kv[0] {
				case "bitrate":
					// print("\033[2J\033[1;1H")
					progress.BitRate = strings.TrimSpace(kv[1])
				case "total_size":
					size, _ := strconv.ParseInt(kv[1], 10, 64)
					progress.TotalSize = size
				case "out_time_us":
					us, _ := strconv.ParseInt(kv[1], 10, 64)
					progress.OutTimeus = us
				case "out_time_ms":
					ms, _ := strconv.ParseInt(kv[1], 10, 64)
					progress.OutTimems = ms
				case "out_time":
					progress.OutTime = kv[1]
				case "dup_frames":
					dup, _ := strconv.ParseUint(kv[1], 10, 64)
					progress.DupFrames = dup
				case "drop_frames":
					drop, _ := strconv.ParseUint(kv[1], 10, 64)
					progress.DropFrames = drop
				case "speed":
					speed, _ := strconv.ParseFloat(
						strings.TrimSuffix(
							strings.TrimSpace(kv[1]),
							"x",
						),
						64,
					)

					progress.Speed = speed
				case "progress":
					switch kv[1] {
					case string(ProgressContinue):
						progress.Progress = ProgressContinue
					case string(ProgressEnd):
						progress.Progress = ProgressEnd
					default:
						progress.Progress = ProgressUnknown
						log.Println("Unknown progress:", kv[1])
					}
				}
			}
		}

		if err := scanner.Err(); err != nil {
			log.Println(err)
		}
	}()

	if err := trans.Start(); err != nil {
		panic(err)
	}

	if resp != nil {
		stdin, err := trans.StdinPipe()
		if err != nil {
			panic(err)
		}

		if total, err := strconv.ParseInt(resp.Header.Get("Content-Length"), 10, 64); err == nil {
			var (
				current int64
				last    int64
			)

			pr := &ProgressReader{resp.Body, func(r int64) {
				current += r
				if r > 0 {
					now := current * 100 / total
					if now != last {
						printfn("\r\033[K")
						last = now
						printfn("Progress: %d%%, encoding: %s", now, progress.OutTime)
					}
				} else {
					printfn("done\n")
				}
			}}

			io.Copy(stdin, pr)

		} else {
			io.Copy(stdin, resp.Body)
		}

		stdin.Close()
	}

	var done bool

	go func() {
		io.Copy(writeToMe, stdout)

		if err := trans.Wait(); err != nil {
			panic(err)
		}

		done = true
	}()

	tick := time.NewTicker(time.Second * 1)

	for {
		<-tick.C
		if done {
			printfn("\n")
			break
		} else {
			printfn("\r\033[K")
			if resp != nil {
				printfn("Finished downloading, ")
			}

			printfn("encoding: %s", progress.OutTime)
		}
	}
}
