package main

import (
	"io"
	"os"
	"sync"
	"syscall"

	"github.com/rylio/ytdl"
)

func FIFOVideo(path string, video *ytdl.VideoInfo, wg *sync.WaitGroup, fmts ...ytdl.Format) {
	defer wg.Done()

	os.Remove(path)

	if err := syscall.Mkfifo(path, 0664); err != nil {
		safePanic(err)
	}

	pipe, err := os.OpenFile(path, os.O_RDWR, os.ModeNamedPipe)
	if err != nil {
		safePanic(err)
	}

	defer pipe.Close()

	video.Formats.Sort(ytdl.FormatResolutionKey, true)
	formats := video.Formats

	if len(fmts) != 1 {
		if VideoQuality != "best" {
			formats = formats.Filter(
				ytdl.FormatResolutionKey,
				[]interface{}{VideoQuality},
			)
		}

		if len(formats) < 1 {
			formats = video.Formats
		}
	} else {
		formats = fmts
	}

	body := downloadFormat(video, formats[0])

	defer body.Close()

	io.Copy(pipe, body)
}

func FIFOAudio(path string, video *ytdl.VideoInfo, wg *sync.WaitGroup, fmts ...ytdl.Format) {
	defer wg.Done()

	os.Remove(path)

	if err := syscall.Mkfifo(path, 0664); err != nil {
		safePanic(err)
	}

	pipe, err := os.OpenFile(path, os.O_RDWR, os.ModeNamedPipe)
	if err != nil {
		safePanic(err)
	}

	defer pipe.Close()

	formats := video.Formats

	if len(fmts) > 1 {
		formats = fmts
	}

	formats.Sort(ytdl.FormatAudioBitrateKey, true)

	body := downloadFormat(video, formats[0])

	defer body.Close()

	io.Copy(pipe, body)
}
