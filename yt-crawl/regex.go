package ytcr

import "regexp"

var (
	// TitleAndLink returns 2 capture groups, the first being the relative link and second being the title
	TitleAndLink = regexp.MustCompile(`<a href="(.*?)" class="yt-uix-tile-link yt-ui-ellipsis yt-ui-ellipsis-2 yt-uix-sessionlink      spf-link " .*? title="(.*?)" .*? dir="ltr">`)

	// Description is the regex to capture description into a capture group
	Description = regexp.MustCompile(`<div class="yt-lockup-description yt-ui-ellipsis yt-ui-ellipsis-2" dir="ltr">(.*?)</div>`)

	// TimeAndViews returns 2 capture groups, the first being Time uploaded and second being Views
	TimeAndViews = regexp.MustCompile(`<ul class="yt-lockup-meta-info"><li>(.*?)</li><li>(.*?)</li></ul>`)

	// HTMLTags All HTML tags
	HTMLTags = regexp.MustCompile(`<[^>]*>`)
)

// Result is a struct for one result.
// Note that none of these fields are parsed, except for
// Description, having all its HTML tags stripped
type Result struct {
	Title       string
	Description string

	Link string // https://youtube.com/watch?v=ID

	Time  string // 5 months ago
	Views string // 3,463,851 views
}
