package ytcr

import (
	"testing"

	"github.com/davecgh/go-spew/spew"
)

func TestSearch(t *testing.T) {
	res, err := Search("poppy diplo time is up")
	if err != nil {
		t.Fatal(err)
	}

	spew.Dump(res)
}
