package ytcr

import (
	"html"
	"io/ioutil"
	"net/http"
	"net/url"
)

// CraftSearchURL makes a search URL
func CraftSearchURL(query string) string {
	u, _ := url.ParseRequestURI("https://www.youtube.com/results")

	v := u.Query()
	v.Set("search_query", query)
	// Todo: val.Set("page", "")

	u.RawQuery = v.Encode()

	return u.String()
}

// Search searches up for query and crawl it
func Search(query string) (res []Result, err error) {
	if query == "" {
		return
	}

	resp, err := http.Get(CraftSearchURL(query))
	if err != nil {
		return
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	titlelink := TitleAndLink.FindAllStringSubmatch(
		string(body), -1,
	)

	for _, v := range titlelink {
		res = append(res, Result{
			Title: html.UnescapeString(v[2]),
			Link:  "https://youtube.com" + v[1],
		})
	}

	descriptions := Description.FindAllStringSubmatch(
		string(body), -1,
	)

	for i, v := range descriptions {
		desc := HTMLTags.ReplaceAllString(v[1], "")
		res[i].Description = html.UnescapeString(desc)
	}

	timeviews := TimeAndViews.FindAllStringSubmatch(
		string(body), -1,
	)

	for i, v := range timeviews {
		res[i].Time = v[1]
		res[i].Views = v[2]
	}

	return
}
