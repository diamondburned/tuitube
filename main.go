package main

import (
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"sync"

	"github.com/gdamore/tcell"
	"github.com/rivo/tview"
	"github.com/rylio/ytdl"
	browser "gitlab.com/diamondburned/pkg-browser"
	ytcr "gitlab.com/diamondburned/tuitube/yt-crawl"
)

var (
	VideoQuality = "720p"

	app     = tview.NewApplication()
	results = tview.NewList()
)

func init() {
	if v := os.Getenv("PLAYER"); v != "" {
		PreferredPlayer = v
	}
}

func main() {
	a := flag.Bool("a", false, "Downloads audio only")
	f := flag.String("f", "", "Filters")
	flag.Parse()

	if *a || *f != "" {
		if len(flag.Args()) < 2 {
			println("Invalid args! Usage: ./ytdl -a $VIDEOURL $DESTINATION")
		}

		u := flag.Args()[0]

		var filename = flag.Args()[1]

		v, err := ytdl.GetVideoInfo(u)
		if err != nil {
			panic(err)
		}

		switch {
		case *a:
			v.Formats.Sort(
				ytdl.FormatAudioBitrateKey,
				true,
			)

			if s, err := os.Stat(filename); err == nil {
				if s.Mode().IsDir() {
					var fmt string
					switch v.Formats[0].AudioEncoding {
					case "opus", "vorbis":
						fmt = "ogg"
					default:
						fmt = "m4a"
					}

					filename = filepath.Join(
						filename,
						v.Title+"."+fmt,
					)
				}

				os.Remove(filename)
			}

			trans := exec.Command(
				"ffmpeg", "-y",
				"-nostdin",
				"-i", "-",
				"-c:a", "copy",
				"-progress", "pipe:2",
				"-f", "ogg",
				"-",
			)

			url, err := v.GetDownloadURL(v.Formats[0])
			if err != nil {
				panic(err)
			}

			resp, err := http.Get(url.String())
			if err != nil {
				panic(err)
			}

			encodeMe(trans, resp, filename)
		case *f != "":
			fmts := v.Formats

			flags := strings.Split(*f, ",")
			for _, f := range flags {
				_s := strings.Split(f, ":")
				if len(_s) != 2 {
					println("Invalid flag: " + f)
					continue
				}

				fmts = fmts.Filter(
					ytdl.FormatKey(_s[0]),
					[]interface{}{_s[1]},
				)
			}

			if len(fmts) < 1 {
				println("Can't match all filters")
				os.Exit(1)
			}

			if s, err := os.Stat(filename); err == nil {
				if s.Mode().IsDir() {
					filename = filepath.Join(
						filename,
						v.Title+".mkv",
					)
				}

				os.Remove(filename)
			}

			var (
				audiofifo = "/tmp/audio" + v.ID
				videofifo = "/tmp/video" + v.ID
				wg        = &sync.WaitGroup{}
			)

			wg.Add(2)

			go FIFOAudio(audiofifo, v, wg, fmts...)
			go FIFOVideo(videofifo, v, wg, fmts[0])

			trans := exec.Command(
				"ffmpeg", "-y",
				"-nostdin",
				"-i", videofifo,
				"-i", audiofifo,
				"-progress", "pipe:2",
				"-f", "matroska",
				"-",
			)

			encodeMe(trans, nil, filename)

			wg.Wait()
		}

		os.Exit(0)
	}

	results.ShowSecondaryText(true)
	results.SetSelectedFocusOnly(true)
	results.SetBackgroundColor(tcell.ColorBlack)
	results.SetMainTextColor(tcell.ColorWhite)
	results.SetSecondaryTextColor(tcell.ColorWhite)
	results.SetSelectedBackgroundColor(tcell.ColorWhite)
	results.SetSelectedTextColor(tcell.ColorBlack)

	flex := tview.NewFlex()
	flex.SetDirection(tview.FlexRow)
	flex.SetBackgroundColor(tcell.ColorDefault)

	{ // Top search part
		searchbox := tview.NewFlex()
		searchbox.SetDirection(tview.FlexColumn)
		searchbox.SetBackgroundColor(tcell.ColorDefault)

		search := tview.NewInputField()
		search.SetPlaceholder("Search...")
		search.SetBackgroundColor(tcell.ColorDefault)

		search.SetDoneFunc(func(k tcell.Key) {
			switch k {
			case tcell.KeyEnter:
				results.Clear()

				res, err := ytcr.Search(search.GetText())
				if err != nil {
					safePanic(err)
				}

				for _, r := range res {
					results.AddItem(
						fmt.Sprintf("%s - %s / %s", r.Title, r.Time, r.Views),
						r.Description,
						' ',
						nil,
					)
				}

				results.SetSelectedFunc(func(i int, main, sub string, r rune) {
					go play(res[i].Link)
				})

				app.Draw()

				results.SetInputCapture(func(ev *tcell.EventKey) *tcell.EventKey {
					switch {
					case ev.Key() == tcell.KeyRight:
						url := res[results.GetCurrentItem()].Link
						if err := browser.OpenURL(url); err != nil {
							safePanic(err)
						}
					case ev.Rune() == 'q':
						app.Stop()
					}

					return ev
				})
			}
		})

		search.SetInputCapture(func(ev *tcell.EventKey) *tcell.EventKey {
			switch ev.Key() {
			case tcell.KeyDown:
				app.SetFocus(results)
			}

			return ev
		})

		searchbox.AddItem(search, 0, 2, true)
		addPrimitive(search)

		dropdown := tview.NewDropDown()
		dropdown.SetBackgroundColor(tcell.ColorDefault)

		dropdown.SetOptions(
			[]string{
				"best",
				"1080p",
				"720p",
				"480p",
				"360p",
				"240p",
				"144p",
			}, func(text string, i int) {
				VideoQuality = text
			},
		)

		dropdown.SetCurrentOption(2)

		searchbox.AddItem(dropdown, 7, 1, true)
		addPrimitive(dropdown)

		searchframe := tview.NewFrame(searchbox)
		searchframe.AddText("Options", true, tview.AlignLeft, tcell.ColorWhite)
		searchframe.SetBorders(0, 0, 1, 0, 1, 1)
		searchframe.SetBackgroundColor(tcell.ColorDefault)

		flex.AddItem(searchframe, 3, 1, true)
	}

	{ // Lower results box
		// results.SetDirection(tview.FlexRow)

		resframe := tview.NewFrame(results)
		resframe.AddText("Results", true, tview.AlignLeft, tcell.ColorWhite)
		resframe.SetBorders(1, 0, 1, 0, 1, 1)
		resframe.SetBackgroundColor(tcell.ColorDefault)

		flex.AddItem(resframe, 0, 2, true)
		addPrimitive(results)
	}

	app.SetInputCapture(func(ev *tcell.EventKey) *tcell.EventKey {
		switch ev.Key() {
		case tcell.KeyEscape:
			app.Stop()
		case tcell.KeyTab:
			cyclePrimitive()
		}

		return ev
	})

	if err := app.SetRoot(flex, true).Run(); err != nil {
		panic(err)
	}
}
